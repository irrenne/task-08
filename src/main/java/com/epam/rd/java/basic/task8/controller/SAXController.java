package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.Flower;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController {

    private final String xmlFileName;

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public List<Flower> parseSAX() throws Exception {
        SAXParserFactory parserFactor = SAXParserFactory.newInstance();
        SAXParser parser = parserFactor.newSAXParser();
        SAXHandler handler = new SAXHandler();
        parser.parse(xmlFileName, handler);

        return handler.empList;
    }

    public void writeToXml(String outputFile, List<Flower> list) {
        GeneralXML w = new GeneralXML();
        w.writeToXml(outputFile, list);
    }

    public void sortBySoilMultiplying(List<Flower> list) {
        Comparator<Flower> bySoil = Comparator.comparing(Flower::getSoil);
        Comparator<Flower> byMultiplying = Comparator.comparing(Flower::getMultiplying);
        list.sort(bySoil.thenComparing(byMultiplying));
    }
}

/**
 * The Handler for SAX Events.
 */
class SAXHandler extends DefaultHandler {

    List<Flower> empList = new ArrayList<>();
    Flower emp = null;
    String content = null;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes)
            throws SAXException {

        switch (qName) {
            case "flower":
                emp = new Flower();
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        switch (qName) {
            case "flower":
                empList.add(emp);
                break;
            case "name":
                emp.setName(content);
                break;
            case "soil":
                emp.setSoil(content);
                break;
            case "origin":
                emp.setOrigin(content);
                break;
            case "multiplying":
                emp.setMultiplying(content);
                break;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length)
            throws SAXException {
        content = String.copyValueOf(ch, start, length).trim();
    }
}