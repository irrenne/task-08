package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

    private final String xmlFileName;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public List<Flower> parseDom() throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        DocumentBuilder builder = factory.newDocumentBuilder();

        Document document = builder.parse(new File(xmlFileName));
        document.getDocumentElement().normalize();

        List<Flower> empList = new ArrayList<>();

        NodeList nodeList = document.getDocumentElement().getChildNodes();

        for (int i = 0; i < nodeList.getLength(); i++) {

            Node node = nodeList.item(i);
            if (node instanceof Element) {
                Flower emp = new Flower();

                NodeList childNodes = node.getChildNodes();
                for (int j = 0; j < childNodes.getLength(); j++) {
                    Node cNode = childNodes.item(j);

                    if (cNode instanceof Element) {
                        String content = cNode.getLastChild().
                                getTextContent().trim();
                        switch (cNode.getNodeName()) {
                            case "name":
                                emp.setName(content);
                                break;
                            case "soil":
                                emp.setSoil(content);
                                break;
                            case "origin":
                                emp.setOrigin(content);
                                break;
                            case "multiplying":
                                emp.setMultiplying(content);
                                break;
                        }
                    }
                    empList.add(emp);
                }
            }
        }
        return empList;
    }

    public void writeToXml(String outputFile, List<Flower> list) throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("flowers");
        rootElement.setAttribute("xmlns", "http://www.nure.ua");
        rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        rootElement.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");
        doc.appendChild(rootElement);

        for (Flower value : list) {

            Element flower = doc.createElement("flower");
            rootElement.appendChild(flower);

            Element name = doc.createElement("name");
            name.setTextContent(value.getName());
            flower.appendChild(name);

            Element soil = doc.createElement("soil");
            soil.setTextContent(value.getSoil());
            flower.appendChild(soil);

            Element origin = doc.createElement("origin");
            origin.setTextContent(value.getOrigin());
            flower.appendChild(origin);

            Element multiplying = doc.createElement("multiplying");
            multiplying.setTextContent(value.getMultiplying());
            flower.appendChild(multiplying);
        }

        try (FileOutputStream output =
                     new FileOutputStream(outputFile)) {
            writeXml(doc, output);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeXml(Document doc, OutputStream output) throws TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();

        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(output);

        transformer.transform(source, result);
    }

    public void sortByName(List<Flower> list) {
        Comparator<Flower> byAge = Comparator.comparing(Flower::getName);
        list.sort(byAge);
    }
}