package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.Flower;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class GeneralXML {

    public void writeToXml(String outputFile, List<Flower> list) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            writeXml(out, list);

            String xml = out.toString(StandardCharsets.UTF_8);

            String prettyPrintXML = formatXML(xml);

            Files.writeString(Paths.get(outputFile),
                    prettyPrintXML, StandardCharsets.UTF_8);

        } catch (TransformerException | XMLStreamException | IOException e) {
            e.printStackTrace();
        }

    }

    private void writeXml(OutputStream out, List<Flower> list) throws XMLStreamException {
        XMLOutputFactory output = XMLOutputFactory.newInstance();

        XMLStreamWriter writer = output.createXMLStreamWriter(out);

        writer.writeStartDocument("utf-8", "1.0");

        writer.writeStartElement("flowers");
        writer.writeAttribute("xmlns", "http://www.nure.ua");
        writer.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        writer.writeAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");

        for (Flower flower : list) {
            writer.writeStartElement("flower");

            writer.writeStartElement("name");
            writer.writeCharacters(flower.getName());
            writer.writeEndElement();

            writer.writeStartElement("soil");
            writer.writeCharacters(flower.getSoil());
            writer.writeEndElement();

            writer.writeStartElement("origin");
            writer.writeCharacters(flower.getOrigin());
            writer.writeEndElement();

            writer.writeStartElement("multiplying");
            writer.writeCharacters(flower.getMultiplying());
            writer.writeEndElement();

            writer.writeEndElement();
        }

        writer.writeEndDocument();

        writer.flush();
        writer.close();
    }

    private String formatXML(String xml) throws TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();

        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        StreamSource source = new StreamSource(new StringReader(xml));
        StringWriter output = new StringWriter();
        transformer.transform(source, new StreamResult(output));

        return output.toString();
    }
}
