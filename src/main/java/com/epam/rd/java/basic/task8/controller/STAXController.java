package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.Flower;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.stream.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private final String xmlFileName;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public List<Flower> parseStax() throws Exception {
        List<Flower> empList = null;
        Flower currEmp = null;
        String tagContent = null;
        Reader fileReader = new FileReader(xmlFileName);
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(fileReader);

        while (reader.hasNext()) {
            int event = reader.next();

            switch (event) {
                case XMLStreamConstants.START_ELEMENT:
                    if ("flower".equals(reader.getLocalName())) {
                        currEmp = new Flower();
                    }
                    if ("flowers".equals(reader.getLocalName())) {
                        empList = new ArrayList<>();
                    }
                    break;

                case XMLStreamConstants.CHARACTERS:
                    tagContent = reader.getText().trim();
                    break;

                case XMLStreamConstants.END_ELEMENT:
                    switch (reader.getLocalName()) {
                        case "flower":
                            if (empList != null) {
                                empList.add(currEmp);
                            }
                            break;
                        case "name":
                            if (currEmp != null) {
                                currEmp.setName(tagContent);
                            }
                            break;
                        case "soil":
                            if (currEmp != null) {
                                currEmp.setSoil(tagContent);
                            }
                            break;
                        case "origin":
                            if (currEmp != null) {
                                currEmp.setOrigin(tagContent);
                            }
                            break;
                        case "multiplying":
                            if (currEmp != null) {
                                currEmp.setMultiplying(tagContent);
                            }
                            break;
                    }
                    break;

                case XMLStreamConstants.START_DOCUMENT:
                    empList = new ArrayList<>();
                    break;
            }

        }
        return empList;
    }

    public void writeToXml(String outputFile, List<Flower> list) {
        GeneralXML w = new GeneralXML();
        w.writeToXml(outputFile, list);
    }

    public void sortByOrigin(List<Flower> list) {
        Comparator<Flower> byOrigin = Comparator.comparing(Flower::getOrigin);
        list.sort(byOrigin);
    }
}