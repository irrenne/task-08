package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.model.Flower;

import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            return;
        }
        String xmlFileName = args[0];
        System.out.println("Input ==> " + xmlFileName);

        ////////////////////////////////////////////////////////
        // DOM
        ////////////////////////////////////////////////////////

        // get container
        DOMController domController = new DOMController(xmlFileName);
        List<Flower> containerDom = domController.parseDom();

        // sort (case 1)
        domController.sortByName(containerDom);

        // save
        String outputXmlFile = "output.dom.xml";
        domController.writeToXml(outputXmlFile, containerDom);

        ////////////////////////////////////////////////////////
        // SAX
        ////////////////////////////////////////////////////////

        // get
        SAXController saxController = new SAXController(xmlFileName);
        List<Flower> containerSax = saxController.parseSAX();

        // sort  (case 2)
        saxController.sortBySoilMultiplying(containerSax);

        // save
        outputXmlFile = "output.sax.xml";
        saxController.writeToXml(outputXmlFile, containerSax);

        ////////////////////////////////////////////////////////
        // StAX
        ////////////////////////////////////////////////////////

        // get
        STAXController staxController = new STAXController(xmlFileName);
        List<Flower> containerStax = staxController.parseStax();

        // sort  (case 3)
        staxController.sortByOrigin(containerStax);

        // save
        outputXmlFile = "output.stax.xml";
        staxController.writeToXml(outputXmlFile, containerStax);
    }
}
